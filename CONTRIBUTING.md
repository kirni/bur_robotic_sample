# Reporting bugs
If you encounter a bug, create a new issue with the **bug** template. It is very
important to add all the used input files to the bug report. In the **bugs**
issueboard you can see the status of all bugs which are visible to you.

# Requesting features
analog to bugs, but with the **feature** template
