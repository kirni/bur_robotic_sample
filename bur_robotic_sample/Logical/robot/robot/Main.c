
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	if(mpRobot.Override == 0)
	{
		mpRobot.Override = 100;
	}
}

void _CYCLIC ProgramCyclic(void)
{
	mpRobot.Enable = 1;
	mpRobot.MpLink = &g4AxRobA;
	mpRobot.Parameters = &robotPram;	
	MpRoboArm4Axis(&mpRobot);		
	
	fbTeach.Enable = 1;
	fbTeach.szDevice = (UDINT)szDevice;
	fbTeach.szFile = (UDINT)szFile;
	fbTeach.X = mpRobot.X;
	fbTeach.Y = mpRobot.Y;
	fbTeach.Z = mpRobot.Z;
	fbTeach.C = mpRobot.C;
	Teach(&fbTeach);
	
	/*toggle gripper once when Mfunction is called*/
	if(fbMFunction.Valid == 1 && fbMFunction.Value == 1 && fbMFunction.Reset == 0)
	{
		doGripper = !doGripper;
		
		//reset Mfucntion
		fbMFunction.Reset = 1;
	}
	
	if(fbMFunction.Value == 0)
	{
		fbMFunction.Reset = 0;
	}
	
	vis[0] = mpRobot.X;
	vis[1] = mpRobot.Y;
	vis[2] = mpRobot.Z;
	vis[3] = mpRobot.C;
}

void _EXIT ProgramExit(void)
{
	mpRobot.Enable = 0;
	MpRoboArm4Axis(&mpRobot);	
	
	fbMFunction.Enable = 0;
	MC_BR_MFunction(&fbMFunction);
}

