
TYPE
	TeachStep_enum : 
		(
		stTEACH_DISABLED := 0,
		stTEACH_WAIT,
		stTEACH_INFO,
		stTEACH_CREATE,
		stTEACH_OPEN,
		stTEACH_WRITE,
		stTEACH_CLOSE,
		stTEACH_DONE,
		stTEACH_ERROR
		);
	TeachInternal_typ : 	STRUCT 
		Step : TeachStep_enum;
		fbClose : FileClose;
		fbWrite : FileWrite;
		fbOpen : FileOpen;
		fbCreate : FileCreate;
		fbInfo : FileInfo;
		Info : fiFILE_INFO;
		Ident : UDINT;
		Offset : UDINT;
		szLine : STRING[80];
		Lenght : UDINT;
	END_STRUCT;
END_TYPE
