
#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif
	#include "TeachLib.h"
#ifdef __cplusplus
	};
#endif

void generateLine(struct Teach* inst)
{
	/*generate <<Mii\n>>*/
	if(inst->TeachMfunction == 1)
	{
		char szIndex[12] = "";
					
		strcpy((UDINT)inst->Internal.szLine, (UDINT)"M");
		
		itoa(inst->Mfunction, (UDINT)szIndex);
		strcat((UDINT)inst->Internal.szLine, (UDINT)szIndex);
		
		strcat((UDINT)inst->Internal.szLine, (UDINT)"\n");
	}
	/*generate <<G00 Xii Yii Zii\n>>*/
	else if(inst->TeachPosition == 1)
	{
		char szPosition[36] = "";
					
		strcpy((UDINT)inst->Internal.szLine, (UDINT)"G00");
		
		ftoa(inst->X, (UDINT)szPosition);
		strcat((UDINT)inst->Internal.szLine, (UDINT)" X");
		strcat((UDINT)inst->Internal.szLine, (UDINT)szPosition);
		
		ftoa(inst->Y, (UDINT)szPosition);
		strcat((UDINT)inst->Internal.szLine, (UDINT)" Y");
		strcat((UDINT)inst->Internal.szLine, (UDINT)szPosition);
		
		ftoa(inst->Z, (UDINT)szPosition);
		strcat((UDINT)inst->Internal.szLine, (UDINT)" Z");
		strcat((UDINT)inst->Internal.szLine, (UDINT)szPosition);
		
		ftoa(inst->C, (UDINT)szPosition);
		strcat((UDINT)inst->Internal.szLine, (UDINT)" C");
		strcat((UDINT)inst->Internal.szLine, (UDINT)szPosition);
		
		strcat((UDINT)inst->Internal.szLine, (UDINT)"\n");
	}
	
	inst->Internal.Lenght = strlen(inst->Internal.szLine);
}

/* TODO: Add your comment here */
void Teach(struct Teach* inst)
{
	//for nicer code
	TeachInternal_typ* this = &inst->Internal;
	
	//busy on default; status is set in statemachine, except for an error
	if(this->Step != stTEACH_ERROR)
	{
		inst->Status = ERR_FUB_BUSY;
	}
	
	/*reset outputs, they are set within the statemachine*/
	inst->Ready = 0;
	
	/*main statemachine*/
	switch(this->Step)
	{
		case stTEACH_DISABLED:
			
			if(inst->Enable == 1)
			{
				this->Step = stTEACH_WAIT;				
			}
			else
			{
				inst->Status = ERR_FUB_ENABLE_FALSE;
			}
			break;
		
		case stTEACH_WAIT:
			
			//fub is ready for commands
			inst->Ready = 1;
			
			if(inst->TeachMfunction == 1 || inst->TeachPosition == 1)
			{
				//generate line which has to be written
				generateLine(inst);
			
				//get file info
				this->Step = stTEACH_INFO;
			}
			break;
		
		case stTEACH_INFO:
			
			/*setup fub*/
			this->fbInfo.enable = 1;
			this->fbInfo.pDevice = (UDINT)inst->szDevice;
			this->fbInfo.pName = (UDINT)inst->szFile;
			this->fbInfo.pInfo = &this->Info;
			
			//call fub
			FileInfo(&this->fbInfo);
			
			//fub is done..
			if(this->fbInfo.status != ERR_FUB_BUSY)
			{
				//file exists -> open it and append code
				if(this->fbInfo.status == ERR_OK)
				{
					//start writing to the end of the file
					this->Offset = this->Info.size;
					
					//open existing file
					this->Step = stTEACH_OPEN;
				}
				//file does not exist -> create it and insert code
				else if(this->fbInfo.status == fiERR_FILE_NOT_FOUND)
				{
					//start writing at the beginning of the file
					this->Offset = this->Info.size;
					
					//crete new file
					this->Step = stTEACH_CREATE;
				}
				//error
				else
				{
					//set error status and goto error state
					inst->Status = this->fbInfo.status;
					this->Step = stTEACH_ERROR;
				}
				
				//disable fub
				this->fbInfo.enable = 0;			
				FileInfo(&this->fbInfo);
			}
			
			break;
		
		case stTEACH_CREATE:
			
			/*setup fub*/
			this->fbCreate.enable = 1;
			this->fbCreate.pDevice = (UDINT)inst->szDevice;
			this->fbCreate.pFile = (UDINT)inst->szFile;
			
			//call fub
			FileCreate(&this->fbCreate);
			
			//fub is done..
			if(this->fbCreate.status != ERR_FUB_BUSY)
			{
				//success
				if(this->fbCreate.status == ERR_OK)
				{
					this->Ident = this->fbCreate.ident;
					
					//open existing file
					this->Step = stTEACH_WRITE;
				}			
				//error
				else
				{
					//set error status and goto error state
					inst->Status = this->fbCreate.status;
					this->Step = stTEACH_ERROR;
				}
				
				//disable fub
				this->fbCreate.enable = 0;			
				FileCreate(&this->fbCreate);
			}
			
			break;
		
		case stTEACH_OPEN:
			
			/*setup fub*/
			this->fbOpen.enable = 1;
			this->fbOpen.pDevice = (UDINT)inst->szDevice;
			this->fbOpen.pFile = (UDINT)inst->szFile;
			this->fbOpen.mode = fiREAD_WRITE;
			
			//call fub
			FileOpen(&this->fbOpen);
			
			//fub is done..
			if(this->fbOpen.status != ERR_FUB_BUSY)
			{
				//success
				if(this->fbOpen.status == ERR_OK)
				{
					this->Ident = this->fbOpen.ident;
					
					//open existing file
					this->Step = stTEACH_WRITE;
				}			
				//error
				else
				{
					//set error status and goto error state
					inst->Status = this->fbOpen.status;
					this->Step = stTEACH_ERROR;
				}
				
				//disable fub
				this->fbOpen.enable = 0;			
				FileOpen(&this->fbOpen);
			}
			break;
		
		case stTEACH_WRITE:
			
			/*setup fub*/
			this->fbWrite.enable = 1;
			this->fbWrite.ident = this->Ident;
			this->fbWrite.offset = this->Offset;
			this->fbWrite.pSrc = this->szLine;
			this->fbWrite.len = this->Lenght;
			
			//call fub
			FileWrite(&this->fbWrite);
			
			//fub is done..
			if(this->fbWrite.status != ERR_FUB_BUSY)
			{
				//success
				if(this->fbWrite.status == ERR_OK)
				{
					this->Ident = this->fbWrite.ident;
					
					//Write existing file
					this->Step = stTEACH_CLOSE;
				}			
					//error
				else
				{
					//set error status and goto error state
					inst->Status = this->fbWrite.status;
					this->Step = stTEACH_ERROR;
				}
				
				//disable fub
				this->fbWrite.enable = 0;			
				FileWrite(&this->fbWrite);
			}
			break;
		
		case stTEACH_CLOSE:
			
			/*setup fub*/
			this->fbClose.enable = 1;
			this->fbClose.ident = this->Ident;
			
			//call fub
			FileClose(&this->fbClose);
			
			//fub is done..
			if(this->fbClose.status != ERR_FUB_BUSY)
			{
				//success
				if(this->fbClose.status == ERR_OK)
				{
					this->Ident = this->fbClose.ident;
					
					//wait for commands to be reset
					this->Step = stTEACH_DONE;
				}			
					//error
				else
				{
					//set error status and goto error state
					inst->Status = this->fbClose.status;
					this->Step = stTEACH_ERROR;
				}
				
				//disable fub
				this->fbClose.enable = 0;			
				FileClose(&this->fbClose);
			}
			break;
		
		case stTEACH_DONE:
			if(inst->TeachMfunction == 0 && inst->TeachPosition == 0)
			{
				this->Step = stTEACH_WAIT;
			}
			break;
		
		case stTEACH_ERROR:
			//errorhandling
			break;
		
		default:
			this->Step = stTEACH_ERROR;
			break;
	}
}
