# bur_robotic_sample

Sample implementation of a robot with B&R and mapp

# getting started

You need **Automation Studio 4.5** (AS) and **mapp 5.4**. The simulation config **sim** requires the directory **C:/arsim**. Alternatively you may change the file devices in the CPU configuration correspondingly.

After building and transferring to a ARsim, there are two ways of interacting with the robot. First is the mappCockpit. No AS is needed. In order to access the cockpit simply enter this URL into a browser (preferably chrome) **http://localhost:81/index.html?visuId=mappCockpit**. After entering, select the **Automation Component gAxRobA**. After that, all standard functions of a 4 axis robot, such as Power, Home, move, etc. are available. Also trends and a logger for this component are displayed.

The second approach is to control the robot from the watch window in the AS. All functions, such as teaching and jogging are additionally available. For access, open the watch window for the task **robot**. For the functions of mappRobotics, the structures robotParam and mpRobot are relevant.

Since mappRobotics currently does not contain a teach functionality, this is added by the library **TeachLib**. This library contains the function block (**FUB**) Teach, which is also instantiated in the task **robot**. By setting the inputs **TeachPosition** and **TeachMFunction** lines are added to a file defined by **szDevice** and **szFile** correspondingly. If the file is not existing, a new one will be created. The result might look like this:

```
  G00 X2480.61 Y713.179 Z1069.81 C80.2
  G00 X2525.83 Y1134.62 Z673.141 C120.95
  G00 X-2947.72 Y1049.06 Z-40.2439 C-38.48
  G00 X-1853.3 Y-2478.25 Z-160.756 C-2.08
  G00 X555.449 Y-2970.89 Z-223.656 C21.62
  M22
  G00 X1979.46 Y349.033 Z1625 C10
```


Please be aware, that the identifiers for the coordinates and M functions need to fit the definitions of the robot. Currently this is hardcoded. You might edit the library implementation to fit your desired definition, or add parameters for additional flexibility.  
